fs = require('fs');
var parser = require('xml2json');
var shell = require('shelljs');

fs.readFile('./entities.xml', function (err, data) {
    if (err) {
       throw err
        return;
    }

    var json = JSON.parse(parser.toJson(data));
    const subJSON = json["entity-engine-xml"];

    var projectData = subJSON['Project'];
    fs.appendFile('projectData.json','projects = '+ JSON.stringify(projectData) , (error) => {
        if (error)  throw error;
     })

    var userData = subJSON['User'];
    fs.appendFile('userData.json', 'users = '+JSON.stringify(userData) , (error) => {
        if (error)  throw error;
    })

    var issueData = subJSON['Issue'];
    var fileAttachment = subJSON['FileAttachment'];

    for (i in issueData){
        var issue =issueData[i]
        issue.fileList = [];
        for (j in fileAttachment ){
            var file  =fileAttachment[j];
            if (issue.id === file.issue){
                issue.fileList.push({"filename": file.filename , "path" : 'files/'+ issue.projectKey+'/10000/'+issue.projectKey+'-'+issue.number+'/'+file.filename });
                shell.mkdir('-p',  './files/'+ issue.projectKey+'/10000/'+issue.projectKey+'-'+issue.number);
                fs.rename('./attachments/' + issue.projectKey+'/10000/'+issue.projectKey+'-'+issue.number+'/'+file.id ,  'files/'+ issue.projectKey+'/10000/'+issue.projectKey+'-'+issue.number+'/'+file.filename ,  function(err) {
                    if ( err ) console.log('ERROR: ' + err);
                })
            }

        }

    }

    fs.appendFile('issuesData.json', 'issues =  ' +JSON.stringify(issueData) , (error) => {
        if (error)  throw error;
    })
});
