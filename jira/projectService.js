
function load() {
    var headers = ["Id","Name","Key","ProjectType","Lead"];
    var mydata =  projects;
    var table = document.getElementById("table-projects");

   for (var i  in headers) {
        var newHeader = document.createElement('th');
        var headerData = document.getElementsByTagName("th").length
        newHeader.setAttribute('id', headerData);
        newHeader.innerHTML = headers[i]
        table.appendChild(newHeader);
    }

    var newTbody = document.createElement('tbody');
    var tbodyData = document.getElementsByTagName("tbody").length
    newTbody.setAttribute('id', tbodyData);

    for (var i  in mydata) {
        var project = mydata[i];
        var newRow = document.createElement('tr');
        var rowData = document.getElementsByTagName("tr").length
        newRow.setAttribute('id', rowData);
        newRow.setAttribute('class',"table-row");

        newRow.setAttribute('onclick', "loadIssues(\""+project.id+"\", \""+project.name+"\")");

       for (var x in headers) {
            var newColumn = document.createElement('td');
            var columnData = document.getElementsByTagName("td").length
            newColumn.setAttribute('id', columnData);
            // newColumn.setAttribute('value',mydata[i].name);
            newColumn.innerHTML = project[headers[x].toLowerCase()];
            newRow.appendChild(newColumn);
        }
        newTbody.appendChild(newRow);

    }

    table.appendChild(newTbody);
}

load();




var issuesData = issues;


function loadIssues(projectId ,projectName) {

    var projects = document.getElementById("table-projects");
    projects.setAttribute("hidden",true);

    var main = document.getElementById("main");
    var principal = document.createElement("div");
    principal.setAttribute("id","issue-div");

    var titulo = document.createElement("h5");
    titulo.innerHTML="Project  " +projectId +" - " + projectName;

    var back = document.createElement("button");
    back.setAttribute("type","button");
    back.setAttribute("onclick","backToProjectList()");
    back.setAttribute("class","btn btn-outline-dark");
    back.innerHTML="<< back to projects";
    principal.appendChild(back);
    principal.appendChild(document.createElement("br"))
    principal.appendChild(document.createElement("br"))

    principal.appendChild(titulo);

   /* var issueDiv =   document.createElement("div")
    issueDiv.setAttribute("id","issue-canva");
    issueDiv.setAttribute("class","row");

    for ( issue in issuesData){

        if (issuesData[issue].project ===projectId){
            console.log(JSON.stringify(issuesData[issue]));
            var newCard = document.createElement('div');

            newCard.setAttribute('class', "col-md-4");
            newCard.innerHTML = '<div class="card bg-light mb-3">\n' +
                '                        <div class="card-header">Issue: '+ issuesData[issue].projectKey +'-'+issuesData[issue].number+'</div>\n' +
                '                        <div class="card-body">\n' +
                '                            <h5 class="card-title">assignee : '+issuesData[issue].assignee+'</h5>\n' +
                '                            <p class="card-text">'+issuesData[issue].summary+'</p>\n' +
                '                        </div>\n' +
                '                    </div>'


            issueDiv.appendChild(newCard);
        }



    }*/


    var issueDiv =   document.createElement("table")
    issueDiv.setAttribute("id","issue-canva");
    issueDiv.setAttribute("class","table table-hover");

    issueDiv.innerHTML = "<th>issue</th><th>assignee</th><th>summary</th>"
    for ( issue in issuesData){

        if (issuesData[issue].project ===projectId){
            console.log(JSON.stringify(issuesData[issue]));
            var newCard = document.createElement('tr');
            newCard.setAttribute("class","table-row");
            newCard.setAttribute("onclick","loadIssueDetail('"+issuesData[issue].id+"')"),
            newCard.innerHTML =  '<td style="width:10%;">'+ issuesData[issue].projectKey +'-'+issuesData[issue].number+'</td>'  +
                '  <td > '+issuesData[issue].assignee+'</td>' +
                '  <td  >'+issuesData[issue].summary+'</td>';

            issueDiv.appendChild(newCard);
        }
    }
    principal.appendChild(issueDiv);
    main.appendChild(principal);

}

function backToProjectList(){
    var projects = document.getElementById("table-projects");
    projects.removeAttribute("hidden");
    var projects = document.getElementById("issue-div");
    projects.remove();
}

function loadIssueDetail(id) {

    var issues = document.getElementById("issue-div");
    issues.setAttribute("hidden",true);

    var main = document.getElementById("main");
    var issueDiv =   document.createElement("div");
    issueDiv.setAttribute("id","issue-detail");


    var back = document.createElement("button");
    back.setAttribute("type","button");
    back.setAttribute("onclick","backToIssuesList()");
    back.setAttribute("class","btn btn-outline-dark");
    back.innerHTML="<< back to Issues";
    issueDiv.appendChild(back);
    issueDiv.appendChild(document.createElement("br"))
    issueDiv.appendChild(document.createElement("br"))


    for ( issue in issuesData){
        if(issuesData[issue].id ===id){
            var newCard = document.createElement('div');
            var links = "";
            var fileList = issuesData[issue].fileList ;
            for (i in fileList){
                console.log(fileList[i]);
                links+= ' <li><a  class="btn btn-link" href="'+fileList[i].path+'" target="_blank">'+fileList[i].filename+'</a>\n</li>'
            }

            newCard.setAttribute('class', "col-md-10");
            newCard.innerHTML = '<div class="card bg-light mb-3">\n' +
                '                        <div class="card-header">Issue: '+ issuesData[issue].projectKey +'-'+issuesData[issue].number+'</div>\n' +
                '                        <div class="card-body">\n' +
                '                            <h5 class="card-title">assignee : '+issuesData[issue].assignee+'</h5>\n' +
                '                            <p class="card-text">'+issuesData[issue].summary+'</p>\n' +
                '<p class="card-text">'+issuesData[issue].description +'</p>\n' +
                'Attachment List : ' +
                '<ul>'+links+'</ul>' +
                '    </div>\n' +
                '   </div>'


            issueDiv.appendChild(newCard);
        }

    }

    main.appendChild(issueDiv);
}

function backToIssuesList(){
    var projects = document.getElementById("issue-div");
    projects.removeAttribute("hidden");
    var projects = document.getElementById("issue-detail");
    projects.remove();
}

//loadIssueDetail("10737");