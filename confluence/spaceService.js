var spaceList = space;

function loadSpaceList() {
    var headers = ["Name", "Key" ];
    var table = document.getElementById("table-space");

    for (var i  in headers) {
        var newHeader = document.createElement('th');
        var headerData = document.getElementsByTagName("th").length
        newHeader.setAttribute('id', headerData);
        newHeader.innerHTML = headers[i]
        table.appendChild(newHeader);
    }

    var newTbody = document.createElement('tbody');
    var tbodyData = document.getElementsByTagName("tbody").length
    newTbody.setAttribute('id', tbodyData);

    for (var i  in spaceList) {
        var space = spaceList[i];
        var newRow = document.createElement('tr');
        var rowData = document.getElementsByTagName("tr").length
        newRow.setAttribute('id', rowData);
        newRow.setAttribute('class', "table-row");

        newRow.setAttribute('onclick', "loadPages(\""+space.id.$t+"\")");

        for (var x in headers) {

            for (j in space.property) {
                var property = space.property[j];
                if (property.name === headers[x].toLowerCase()) {

                var newColumn = document.createElement('td');
                var columnData = document.getElementsByTagName("td").length
                newColumn.setAttribute('id', columnData);
                // newColumn.setAttribute('value',mydata[i].name);
                newColumn.innerHTML =  property.$t;
                newRow.appendChild(newColumn);
            }
        }


    }
    newTbody.appendChild(newRow);

}

table.appendChild(newTbody);
}

loadSpaceList();


function loadPages(projectId) {
    var pageList = []

    for (p in spaceList){
        if (spaceList[p].id.$t===projectId ){
            pageList = spaceList[p].pageList
        }
    }

    var projects = document.getElementById("table-space");
    projects.setAttribute("hidden",true);

    var main = document.getElementById("main");
    var principal = document.createElement("div");
    principal.setAttribute("id","page-div");

    var titulo = document.createElement("h5");
    titulo.innerHTML="Space  " +projectId +" -  >" ;

    var back = document.createElement("button");
    back.setAttribute("type","button");
    back.setAttribute("onclick","backToProjectList()");
    back.setAttribute("class","btn btn-outline-dark");
    back.innerHTML="<< back to Space List";
    principal.appendChild(back);
    principal.appendChild(document.createElement("br"))
    principal.appendChild(document.createElement("br"))

    principal.appendChild(titulo);

    var issueDiv =   document.createElement("table")
    issueDiv.setAttribute("id","page-canva");
    issueDiv.setAttribute("class","table table-hover");

    var tbody =document.createElement("tbody");
    for ( p in pageList){
           for (x in pageList[p].property){
              var newCard = document.createElement('tr');
              newCard.setAttribute("class","table-row");
              newCard.setAttribute("onclick","loadFileListByPage(\""+projectId+"\",\""+pageList[p].id.$t+"\")");
              if (pageList[p].property[x].name  === "title" ) {
                  var newtd = document.createElement("td");
                  newtd.innerHTML = pageList[p].property[x].$t
                  newCard.appendChild(newtd);
              }
               tbody.appendChild(newCard);
          }


    }
    issueDiv.appendChild(tbody);
    principal.appendChild(issueDiv);
    main.appendChild(principal);

}

function backToProjectList(){
    var projects = document.getElementById("table-space");
    projects.removeAttribute("hidden");
    var projects = document.getElementById("page-div");
    projects.remove();
}

function loadFileListByPage(projectId ,pageId ){

    var pages = document.getElementById("page-div");
    pages.setAttribute("hidden",true);


    var main = document.getElementById("main");
    var principal = document.createElement("div");
    principal.setAttribute("id","attachment-div");

    var titulo = document.createElement("h5");
    titulo.innerHTML="page  " +pageId   ;

    var back = document.createElement("button");
    back.setAttribute("type","button");
    back.setAttribute("onclick","backToPageList()");
    back.setAttribute("class","btn btn-outline-dark");
    back.innerHTML="<< back to Page List";
    principal.appendChild(back);
    principal.appendChild(document.createElement("br"))
    principal.appendChild(document.createElement("br"))

    principal.appendChild(titulo);

    var issueDiv =   document.createElement("table")
    issueDiv.setAttribute("id","page-canva");
    issueDiv.setAttribute("class","table table-hover");

    var tbody =document.createElement("tbody");




    for (s in spaceList){
        var space = spaceList[s];
       if  (space.id.$t=== projectId){
           for ( p in space.pageList){
               var page = space.pageList[p];
               if (page.id.$t === pageId){
                    for (a in page.attachmentList){
                       var tr = document.createElement('tr');
                       tr.setAttribute("class","table-row");

                       var newtd = document.createElement("td");
                       newtd.innerHTML =  '<a  class="btn btn-link" href="'+ page.attachmentList[a].location+'" target="_blank">'+page.attachmentList[a].fileName +'</a>';
                       tr.appendChild(newtd);

                       tbody.appendChild(tr);
                   }
               }
           }
       }
    }


    issueDiv.appendChild(tbody);
    principal.appendChild(issueDiv);
    main.appendChild(principal);

}

function backToPageList(){
    var projects = document.getElementById("page-div");
    projects.removeAttribute("hidden");
    var projects = document.getElementById("attachment-div");
    projects.remove();
}

