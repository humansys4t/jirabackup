fs = require('fs');
var parser = require('xml2json');
var shell = require('shelljs');

fs.readFile('./entities.xml', function (err, data) {
    var json = JSON.parse(parser.toJson(data));
    const subJSON = json["hibernate-generic"];
    const object = subJSON["object"];
    var spaceList = [];

    var pageList = [];

    var attachmentList = [];

    for (i in object) {
        var obj = object[i];
        if (obj.class === "Space") {
            delete obj.collection;
            spaceList.push(obj);
        }

        if (obj.class === "Page") {
            delete obj.collection;
            pageList.push(obj);
        }

        if (obj.class === "Attachment") {
            delete obj.collection;
            attachmentList.push(obj);
        }
    }


    for (i in spaceList) {
        var space = spaceList[i]
        space.pageList = []
        for (j in pageList) {
            var page = pageList[j]
            page.attachmentList = [];
            for (a in attachmentList){
                var attachment = attachmentList[a];
                for (y in attachment.property){
                    var w = attachment.property[y];
                    if (w.class === "Page" && w.id.$t ===page.id.$t ){
                        var file = {}
                        file.fileName =  getTitle(attachment.property);
                        file.location = "files/"+page.id.$t+"/"+attachment.id.$t+"/"+file.fileName
                        page.attachmentList.push(file);

                        shell.mkdir('-p',   "files/"+page.id.$t+"/"+attachment.id.$t);
                        fs.rename('./attachments/'+page.id.$t+"/"+attachment.id.$t+"/1",  "files/"+page.id.$t+"/"+attachment.id.$t+"/"+file.fileName ,  function(err) {
                            if ( err ) console.log('ERROR: ' + err);
                        })
                    }
                }
            }

            if (findCurrent(page.property) && findSpace(page.property, space)) {
                space.pageList.push(page);
            }

        }
    }

    fs.appendFile('space.json', 'space = ' + JSON.stringify(spaceList), (error) => {
        if (error) throw error;
    });


});

function findCurrent(property) {
    var flag = false
    var temp = property.find(function (p) {
        return p.name === "contentStatus" && p.$t === "current"
    })

    if (temp != undefined) {
        flag = true;
    }
    return flag;
}


function findSpace(property, space) {
    var flag = false
    var temp = property.find(function (p) {
        return p.name === "space" && p.id.$t === space.id.$t
    })

    if (temp != undefined) {
        flag = true;
    }
    return flag;
}

function getTitle (property){

    var temp = property.find(function (p ) {
        return p.name === "title"
    })

    return temp.$t
}